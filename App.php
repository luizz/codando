<?php

namespace Codando;

use Symfony\Component\Yaml\Yaml;

/**
 * Controller App
 * @author  Luiz Antônio J. S. Thomas <luizz@luizz.com.br> 
 * @copyright 2014 Luizz
 * @license http://www.luiz.com.br 
 * @version 3.0 
 */
class App {

    public $erro = false;
    public $msg = array();

    /* @var $moduloList <Array>Modulo */
    private $moduloList;

    public function getModulo($modulo = NULL) {

        $is_instanceof = is_instanceof('Codando\Modulo\Modulo', $modulo);

        $modulo = $is_instanceof === FALSE && $modulo == NULL ? input()->_toGet('mod', 'xss%s', FALSE) : $modulo;

        if ($is_instanceof === FALSE && array_key_exists($modulo, $this->moduloList) === TRUE) {

            $yml_modulo = $this->moduloList[$modulo];

            $this->modulo = new Modulo\Modulo;

            $this->modulo->setId($yml_modulo['id'])
                    ->setUrl($modulo)
                    ->setPrimarykey($yml_modulo['primary_key'])
                    ->setTabela($yml_modulo['tabela'])
                    ->setView($yml_modulo['view'])
                    ->setForeign($yml_modulo['foreign'])
                    ->setArquivo(array_key_exists('quantidade', (array) $yml_modulo['arquivo']))
                    ->setClasseNome('\\Codando\\Modulo\\' . $yml_modulo['class']);

            if ($this->modulo->getArquivo()){
                
                $this->modulo->setArquivoQuantidade($yml_modulo['arquivo']['quantidade']);
            }
            
        } else {

            $this->modulo = $modulo;
        }

        return $this->modulo;
    }

    public function loadModulo($modulo = NULL, $where = array()) {

        /* @var $modulo Modulo\Modulo */
        $modulo = $this->getModulo($modulo);

        if (is_instanceof('Codando\Modulo\Modulo', $modulo) === FALSE) {
            $this->erro = TRUE;
            $this->msg[] = 'Modulo Invalido!';
            return NULL;
        }

        $loadModulo = db()->_load($modulo, (array)$where);

        if ($loadModulo != NULL && db()->isErro() === FALSE) {

            $loadModulo = $this->loadArquivo($modulo, $loadModulo);
        } else {

            $this->erro = TRUE;
            $this->msg[] = "ERRO na QUERY SQL [" . db()->getSql() . "]";
        }

        return $loadModulo;
    }

    public function listModulo($modulo = NULL, $where = array(), $limit = NULL, $order = NULL) {

        $where = is_array($where) ? $where:array($where);

        /* @var $modulo Modulo\Modulo */
        $modulo = $this->getModulo($modulo);

        if (is_instanceof('Codando\Modulo\Modulo', $modulo) === FALSE) {
            $this->erro = TRUE;
            $this->msg[] = 'Modulo Invalido!';
            return NULL;
        }

        $lists = db()->_list($modulo, array(), $where, $limit, $order);

        if (db()->isErro() === FALSE) {

            if (count($lists) > 0){

                if($modulo->getArquivo() == 1) {

                    $lists = $this->loadArquivo($modulo, $lists);
                }
            }
            
            
        } else {

            $this->erro = TRUE;
            $this->msg[] = 'Erro na listagem';
            return array();
        }

        return (array) $lists;
    }

    public function countModulo($modulo = NULL, $where = array()) {

        $modulo = $this->getModulo($modulo);

        if (is_instanceof('Codando\Modulo\Modulo', $modulo) === FALSE) {
            $this->erro = TRUE;
            $this->msg[] = 'Modulo Invalido!';
            return NULL;
        }

        $count = db()->count($modulo->getTabela(true), (array)$where);

        return $count;
    }

    private function loadArquivo($modulo, $objeto) {

        if (is_instanceof('Codando\Modulo\Modulo', $modulo) === FALSE) {
            $this->erro = TRUE;
            $this->msg[] = 'Modulo Invalido!';
            return NULL;
        }

        if ($modulo->getArquivo() == 1) {
            if (is_array($objeto) === TRUE) {

                foreach ($objeto as $obj) {
                    $obj = $this->loadArquivo($modulo, $obj);
                }
            } else {

                $objeto = $this->setArquivos($modulo, $objeto);
            }
        }

        return $objeto;
    }

    private function setArquivos($modulo, $objet) {

        if (is_instanceof('Codando\Modulo\Modulo', $modulo) === FALSE) {
            $this->erro = TRUE;
            $this->msg[] = 'Modulo Invalido!';
            return NULL;
        }

        $this->listArquivoObject = array();

        $arquivoModulo = $this->getModulo('arquivo');

        $arquivoList = db()->_list($arquivoModulo, array(), array('id_est = :id_est AND id_modulo = :id_modulo', array('id_est' => $objet->getId(), 'id_modulo' => $modulo->getId())), $modulo->getArquivoQuantidade(), 'ordem DESC');

        unset($arquivoModulo);

        if (count($arquivoList) > 0) {

            $arquivoListTemp = array();

            $countOrdem = 0;

            /* @var $arquivo Arquivo */
            foreach ($arquivoList as $arquivo) {

                $arquivo->setBase($modulo->getUrl());

                $arquivoListTemp[] = $arquivo;

                $countOrdem++;
            }

            if ($modulo->getArquivoQuantidade() > 1 && $arquivoListTemp !== NULL) {

                $objet->setArquivoList($arquivoListTemp);
            } else if (count($arquivoListTemp) == 1 && is_instanceof('Codando\Modulo\Arquivo', current($arquivoListTemp)) === TRUE) {

                $objet->setArquivo(current($arquivoListTemp));
            }

            $this->listArquivoObject = $modulo->getArquivoQuantidade() > 1 ? $arquivoListTemp : (count($arquivoListTemp) == 1 ? current($arquivoListTemp) : new Codando\Modulo\Arquivo());
        }

        return $objet;
    }

    public function clearFix() {

        $this->erro = FALSE;
        $this->modulo = NULL;
        $this->paginacao = array();

        return $this;
    }

    public function getMsg($key = null) {
        if ($key == null)
            return is_array($this->msg) ? implode("<br>", $this->msg) : $this->msg;
        else
            return is_array($this->msg) ? $this->msg[$key] : $this->msg;
    }

    public function __construct() {
        $this->moduloList = \Codando\App::getModels();
    }

    public function __destruct() {
        $this->clearFix();
    }
    
    public static function __callStatic($functionName, $functionParameters){
         
        static $router = array('get'=>array(), 'post'=>array());

        if($functionName == 'run'){
            
            $url = getenv(REQUEST_URI);
            $url = $url === '/' ? 'home':$url;
            
            foreach ($router[getenv(REQUEST_METHOD)] as $pattern => $callback) {

                if (preg_match($pattern, $url, $params)) {
                    array_shift($params);
                    return call_user_func_array($callback, array_values($params));
                }
            }
            
        } else {
            
            $functionParameters[0] = '/' . str_replace('/', '\/', $functionParameters[0]) . '/';
            
            $router[$functionName][$functionParameters[0]] = $functionParameters[1];
        }
    }
    
    /**
     * @return Array
     */
    public static function getModels($name = NULL) {
        static $models = array();

        if ($name !== NULL && array_key_exists($name, $models) === true) {
            return $models[$name];
        }

        return $models ? : $models = Yaml::parse(file_get_contents((COD_DIR_APP . '/_config/modulo.yml')));
    }
    
    /**
     * @return Array
     */
    public static function getCache($name = NULL) {
        
        static $caches = array();

        if ($name !== NULL && array_key_exists($name, $caches) === true) {
            return $caches[$name];
        }
        
        return $caches[$name] = new \Codando\System\Cache(60, COD_DIR_APP . '/_arquivos/appcache/'. $name .'/', 'contr');
        
    }

    /**
     * @return Array
     */
    public static function getConfig($name = NULL) {
        static $config = array();

        if ($name !== NULL && array_key_exists($name, $config) === true) {
            return $config[$name];
        }

        return $config ? : $config = Yaml::parse(file_get_contents(COD_DIR_APP . '/_config/config.yml'));
    }
    
    /**
     * @return Array
     */
    public static function getYml($file, $name = NULL) {
        
        static $yaml = array();
        
        $yaml[$file] ? : $yaml[$file] = Yaml::parse(file_get_contents(COD_DIR_APP . '/_config/'. $file .'.yml'));
        
        if (array_key_exists($file, $yaml) === true) {
            
            if ($name !== NULL && array_key_exists($name, $yaml[$file]) === true) {
                
                return $yaml[$file][$name];
            }
            
            return $yaml[$file];
        }
    }

    /**
     * @return Codando
     */
    public static function getInstance() {
        static $instance = null;

        return $instance ? : $instance = new static;
    }

    private function __clone() {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

    final private function __wakeup() {
        trigger_error('Unserializing is not allowed.', E_USER_ERROR);
    }

}