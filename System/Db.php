<?php

namespace Codando\System;

use \PDO,
    \Codando\Modulo,
    \Doctrine\DBAL;

/**
 * Classe Banco de Dados, com os methods de comunicação com banco de dados.
 * @author  Luiz Antônio J. S. Thomas <luizz@luizz.com.br> 
 * @copyright 2014 Luizz 
 * @license http://www.luiz.com.br 
 * @version 2.0 
 */
class Db {

    private $erro = false;
    private $msg = array();
    /* @var $doctrine \Doctrine\DBAL\Connection */
    private $doctrine;
    private $sql;
    private $cacheFile;
    private static $instance;
    private $exception = array(
        'code-1045' => 'Houve uma falha de comunica&ccedil;&atilde;o com a base de dados usando: \'%1$s\'@\'%2$s\'',
        'code-2002' => 'Nenhuma conex&atilde;o p&ocirc;de ser feita porque a m&aacute;quina de destino as recusou ativamente. Este host n&atilde;o &eacute; conhecido.',
        'code-2005' => 'N&atilde;o houve comunica&ccedil;&atilde;o com o host fornecido. Verifique as suas configura&ccedil;&otilde;es.',
        'no-database' => 'Base de dados desconhecida. Verifique as suas configura&ccedil;&otilde;es.',
        'no-instance' => 'N&atilde;o existe uma inst&acirc;ncia do objeto Code DB dispon&iacute;vel. Imposs&iacute;vel acessar os m&eacute;todos.',
        'no-argument-sql' => 'O argumento SQL de consulta est&aacute; ausente.',
        'no-instruction-json' => 'A instrução SQL no formato JSON est&aacute; ausente.',
        'duplicate-key' => 'N&atilde;o foi poss&iacute;vel gravar o registro. Existe uma chave duplicada na tabela.<br />\'%1$s',
        'critical-error' => 'Erro crítico detectado no sistema.',
        'error-badly-json' => 'A query JSON fornecida est&aacute; mal formatada.',
    );

    /**
     * Conecta no banco
     * Utiliza o construtor da superclasse
     */
    public function __construct() {

        if (is_instanceof('Doctrine\DBAL\Connection', $this->doctrine) === false) {

            $_config = \Codando\App::getConfig('database');

            if (count($_config) >= 6) {

                //$this->cacheFile = new \Doctrine\Common\Cache\FilesystemCache(COD_DIR_APP . '/_arquivos/cache-doctrine/');
                
                try {
                    try {

                        $config = new \Doctrine\DBAL\Configuration();
                        
                        //$config->setResultCacheImpl($this->cacheFile);
                        
                        $connectionParams = array(
                            'dbname' => $_config['name'],
                            'user' => $_config['user'],
                            'password' => $_config['password'],
                            'host' => $_config['host'],
                            'driver' => 'pdo_' . $_config['driver'],
                        );

                        $this->doctrine = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

                        if (is_instanceof('Doctrine\DBAL\Connection', $this->doctrine) === true) {
                            
                        }
                    } catch (\Doctrine\DBAL\DBALException $e) {

                        $error = $this->getErrorInfo($e);

                        if ($error['code'] == '2005')
                            throw new Exception($this->exception['code-2005']);
                        elseif ($error['code'] == '2002')
                            throw new Exception($this->exception['code-2002']);
                        elseif ($error['code'] == '1045')
                            throw new Exception(sprintf($this->exception['code-1045'], 'root', '******'));
                        else
                            throw $e;

                        $this->erro = true;
                        $this->msg[] = $e->getMessage();
                        return NULL;
                    }
                } catch (\Doctrine\DBAL\DBALException $e) {

                    erro($e);
                }
            }
        }

        return $this;
    }

    /**
     * Disconect
     * Quando o objeto for destruido a conexao e fechada
     */
    public function _disconect() {
        $this->doctrine = null;
    }

    /**
     * Destrutor
     * Quando o objeto for destruido a conexao e fechada
     */
    public function _destruct() {
        $this->doctrine = null;
    }

    public function _list($moduloOrTable, $fieldsNames = array(), $fieldsWhereValue = array(), $limit = NULL, $order = NULL) {

        $list = array();

        $is_foreign = is_instanceof('Codando\Modulo\Modulo', $moduloOrTable) && is_array($moduloOrTable->getForeign()) ? $moduloOrTable->getForeign() : NULL;

        $tabela = is_instanceof('Codando\Modulo\Modulo', $moduloOrTable) ? $moduloOrTable->getTabela(true) : $moduloOrTable;

        try {

            $lisTemp = $this->select($tabela, $fieldsNames, current($fieldsWhereValue), end($fieldsWhereValue), $limit, $order);

            if ($lisTemp != NULL && $lisTemp->rowCount() > 0) {

                if (is_instanceof('Codando\Modulo\Modulo', $moduloOrTable) === TRUE) {

                    $lisTemp->setFetchMode(PDO::FETCH_CLASS, $moduloOrTable->getClasseNome());

                    $listForeign = array();

                    //Montar Array foreign
                    if ($is_foreign !== NULL) {

                        $app = \Codando\App::getInstance();
                        $models = \Codando\App::getModels();
                        foreach ($is_foreign as $foreign) {
                            $listForeign[$foreign] = array();
                        }
                    }
                    
                    //Obter foreigns
                    while ($registroObj = $lisTemp->fetch()) {
                        
                        //Obter ID foreign
                        if ($is_foreign !== NULL) {

                            foreach ($is_foreign as $foreign) {
                                
                                if (isset($models[$foreign])) {

                                    $modelYml = $models[$foreign];
                                    $get = "get" . $modelYml['class'];
                                    
                                    if(method_exists($registroObj, $get) === TRUE){
                                        $listForeign[$foreign][$registroObj->getId()] = $registroObj->{$get}();
                                        
                                    } else if(method_exists($registroObj, 'set' . $modelYml['class'] . 'List') === TRUE){
                                 
                                        $listFore = $app->listModulo($foreign, array(
                                                                                        $moduloOrTable->getPrimarykey() . ' = :' . $moduloOrTable->getPrimarykey() , array($moduloOrTable->getPrimarykey() => $registroObj->getId())
                                                                                    )
                                                                    );

                                        if(count($listFore) > 0){
                                            $registroObj->{'set' . $modelYml['class'] . 'List'}($listFore);
                                        }
                                        
                                      
                                    }
                                    
                                }
                            }
                        }
                        
                        if(method_exists($registroObj, 'getId') === TRUE)
                            $list[$registroObj->getId()] = $registroObj;
                        
                    }

                    //Setar foreign's                    
                    foreach ($listForeign as $foreign => $in_array) {

                        if (is_array($in_array)) {
                            
                            $modelYml = $models[$foreign];
                            
                            $set = "set" . $modelYml['class'];
                            
                            if(count($in_array) > 0){
                               
                                $listIn = $app->listModulo($foreign, $modelYml['primary_key'] . ' IN(' . implode(',', array_values($in_array)) . ')');

                                foreach ($in_array as $key => $forId) {
                                    if (isset($listIn[$forId])) {
                                        $list[$key]->{$set}($listIn[$forId]);
                                    }
                                }
                            }
                            
                            $listIn = $modelYml = NULL;
                        }
                    }

                    $listForeign = $models = NULL;
                    
                } else {

                    $list = $lisTemp->fetchAll(PDO::FETCH_NUM);
                }

                $lisTemp = NULL;

                unset($lisTemp);
            }
        } catch (PDOException $e) {

            erro($e);
            $this->erro = true;
            $this->msg[] = $e->getMessage();
        }

        return $list;
    }

    public function _load($moduloOrTable, $fieldsWhereValue = array()) {

        $load = NULL;
        
        $is_foreign = is_instanceof('Codando\Modulo\Modulo', $moduloOrTable) && is_array($moduloOrTable->getForeign()) ? $moduloOrTable->getForeign() : NULL;
        
        $table = is_instanceof('Codando\Modulo\Modulo', $moduloOrTable) ? $moduloOrTable->getTabela(true) : $moduloOrTable;

        try {

            $select = $this->select($table, array(), current($fieldsWhereValue), end($fieldsWhereValue), 1);

            if ($select != NULL) {

                if ($select->rowCount() >= 1) {

                    $load = $select->fetchAll(PDO::FETCH_CLASS, "\\Codando\\Modulo\\" . tableToclass($table));
                    $load = end($load);
                    
                    //Montar Array foreign
                    if ($is_foreign !== NULL) {

                        $app = \Codando\App::getInstance();
                        $models = \Codando\App::getModels();
                        
                        foreach ($is_foreign as $foreign) {
                            
                            if (isset($models[$foreign])) {
                            
                                $modelYml = $models[$foreign];
                                
                                $get = "get" . $modelYml['class'];
                                $set = "set" . $modelYml['class'];
                                            
                                if(method_exists($load, $get) === TRUE && method_exists($load, $set) === TRUE){
                                 
                                    $loadFore = $app->loadModulo($foreign, array(
                                                                                    $modelYml['primary_key'] . ' = :' . $modelYml['primary_key'],
                                                                                    array($modelYml['primary_key'] => $load->{$get}())
                                                                                )
                                                                );
                                  
                                    if(is_instanceof("Codando\Modulo\\".$modelYml['class'], $loadFore)){
                                        $load->{$set}($loadFore);
                                    }
                                } else if(method_exists($load, $set . 'List') === TRUE){
                                 
                                    $listFore = $app->listModulo($foreign, array(
                                                                                    $moduloOrTable->getPrimarykey() . ' = :' . $moduloOrTable->getPrimarykey() , array($moduloOrTable->getPrimarykey() => $load->getId())
                                                                                )
                                                                );
                                  
                                    if(count($listFore) > 0){
                                        $load->{$set. 'List'}($listFore);
                                    }
                                   
                                }
                                
                                $modelYml = $get =  $set = NULL;
                                
                            }
                        }
                    }
                    
                    $is_foreign = $models = NULL;
                    
                    
                    
                } else {

                    $this->erro = true;
                    $this->msg[] = "Quantidade diferente de 1, igual a " . $select->rowCount() . ".";
                }
            }
        } catch (PDOException $e) {
            erro($e);
            $this->erro = true;
            $this->msg[] = $e->getMessage();
        }


        unset($select);

        return $load;
    }

    public function count($table, $fieldsWhereValue = array()) {

        $countTemp = $this->select($table, array('COUNT(*) AS count'), current($fieldsWhereValue), end($fieldsWhereValue));

        if ($countTemp != NULL) {

            $fetchtemp = $countTemp->fetchAll(PDO::FETCH_OBJ);
            $count = end($fetchtemp);
            unset($fetchtemp);

            return $count->count;
        } else {

            return 0;
        }
    }

    public function insert($table, $fieldsNamesOrNamesToValue = array()) {

        if (is_instanceof('Doctrine\DBAL\Connection', $this->doctrine) === FALSE) {
            exit('DBAL Not Connection');
        }

        $this->clearAll();

        $queryBuilder = $this->doctrine->createQueryBuilder()
                ->insert($table);

        $ind = 0;

        foreach ((array) $fieldsNamesOrNamesToValue as $name => $value) {

            $queryBuilder->setValue($name, '?')
                    ->setParameter($ind++, $value);
        }

        if ($queryBuilder->execute() > 0) {

            return $this->doctrine->lastInsertId("id_" . $table);
        }

        return NULL;
    }

    public function update($table, $fieldsValue = array(), $sqlParamWhere = NULL, $valueListParamWhere = array()) {

        if (is_instanceof('Doctrine\DBAL\Connection', $this->doctrine) === FALSE) {
            exit('DBAL Not Connection');
        }

        $this->clearAll();

        $queryBuilder = $this->doctrine->createQueryBuilder()
                ->update($table);

        foreach ((array) $fieldsValue as $name => $value) {

            $queryBuilder->set($name, ':' . $name)->setParameter(':' . $name, $value);
        }

        if (is_string($sqlParamWhere) === true && $sqlParamWhere !== NULL) {

            $queryBuilder->where($sqlParamWhere);

            if (is_array($valueListParamWhere) && count($valueListParamWhere) > 0) {
                foreach ((array) $valueListParamWhere as $name => $value) {
                    $queryBuilder->setParameter(':' . $name, $value);
                }
            }
        }

        if ($queryBuilder->execute() > 0) {

            return true;
        }

        return false;
    }

    public function delete($table, $sqlParamWhere = NULL, $valueListParamWhere = array()) {

        if (is_instanceof('Doctrine\DBAL\Connection', $this->doctrine) === FALSE) {
            exit('DBAL Not Connection');
        }

        $this->clearAll();

        $queryBuilder = $this->doctrine->createQueryBuilder()
                ->delete($table);

        if (is_string($sqlParamWhere) === true && $sqlParamWhere !== NULL) {

            $queryBuilder->where($sqlParamWhere);

            if (is_array($valueListParamWhere) && count($valueListParamWhere) > 0) {

                $queryBuilder->setParameters($valueListParamWhere);
            }
        }

        if ($queryBuilder->execute() > 0) {

            return true;
        }

        return false;
    }

    public function select($table, $fieldsNames = array(), $sqlParamWhere = NULL, $valueListParamWhere = array(), $limit = NULL, $order = NULL) {

        if (is_instanceof('Doctrine\DBAL\Connection', $this->doctrine) === FALSE) {
            exit('DBAL Not Connection');
        }

        $this->clearAll();

        $queryBuilder = $this->doctrine->createQueryBuilder()
                ->select(count($fieldsNames) == 0 ? $table[0] . '.*' : $fieldsNames)
                ->from($table, $table[0]);
        
        //$queryBuilder->useResultCache(true, $this->timecache);
       
        if (is_string($sqlParamWhere) === true && $sqlParamWhere !== NULL) {

            $queryBuilder->where($sqlParamWhere);

            if (is_array($valueListParamWhere) && count($valueListParamWhere) > 0) {

                $queryBuilder->setParameters($valueListParamWhere);
            }
        }

        if ($order !== NULL) {

            $queryBuilder->orderBy($order, ' ');
        }

        if (is_array($limit) === TRUE) {

            $queryBuilder->setFirstResult($limit[0])
                    ->setMaxResults($limit[1]);
        } else if ($limit > 0) {

            $queryBuilder->setMaxResults($limit);
        }

        $this->sql = $queryBuilder->getSQL();
             
        return $queryBuilder->execute();//$this->doctrine->executeQuery($this->sql, $queryBuilder->getParameters(), $queryBuilder->getType(),  new \Doctrine\DBAL\Cache\QueryCacheProfile(3600, md5($this->sql), $this->cacheFile));
    }

    public function getSql() {
        return $this->sql;
    }

    public function isErro() {
        return $this->erro;
    }

    private function clearAll() {
        $this->sql = NULL;
        $this->erro = false;
        return $this;
    }

    private function getErrorInfo(Exception $e, $show = false) {

        $info = null;
        $errorInfo = null;
        $message = $e->getMessage();

        preg_match('~SQLSTATE[[]([[:alnum:]]{1,})[]]:?\s[[]?([[:digit:]]{1,})?[]]?\s?(.+)~', $message, $errorInfo);
        $info['state'] = isset($errorInfo[1]) ? $errorInfo[1] : null;
        $info['code'] = isset($errorInfo[2]) ? $errorInfo[2] : null;
        $info['message'] = isset($errorInfo[3]) ? $errorInfo[3] : null;

        if ($show)
            echo '<pre>', print_r($info), '</pre>';

        try {
            if ($info['state'] == '23000')
                throw new PDOException(sprintf($this->exception['duplicate-key'], $info['message']));
            return $info;
        } catch (PDOException $e) {
            erro($e);
        }

        return $info;
    }

    /*
     * @return Db
     */

    public static function get_db() {

        static $instance = null;

        return $instance ? : $instance = new static;
    }

}
