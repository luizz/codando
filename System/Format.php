<?php
namespace Codando\System;
/*
* @author  Luiz Antônio J. S. Thomas <luizz@luizz.com.br> 
* @copyright 2014 Luizz 
* @license http://www.luiz.com.br 
* @version 2.0 
*/
class Format {

    private static $instance = NULL;

    /**
     * Retorna a data por extenso
     * @param date $date Data a ser convertida no formato DD/MM/YYYY
     * @param boolean $showWeek Informa se deve exibir o dia da semana. True para exibir, False para não exibir (Padrão: False)
     * @param string $separator Separador da data (Default: "/")
     * @return string
     */
    public function dateToString($date, $showWeek = false, $separator = "/") {

        setlocale(LC_TIME, 'ptb', 'pt_BR', 'portuguese-brazil', 'bra', 'brazil', 'pt_BR.utf-8', 'pt_BR.iso-8859-1', 'br');
        $stringConfig = $showWeek === true ? "%A, %d de %B de %Y" : "%d de %B de %Y";

        $arrayData = explode($separator, $date);

        $string = strftime($stringConfig, mktime(0, 0, 0, $arrayData[1], $arrayData[0], $arrayData[2]));

        return $string;
    }

    /**
     * Retorna a data mês e ano por extenso
     * @param date $date Data a ser convertida no formato DD/MM/YYYY
     * @param string $language Sigla do idioma (pt|en|es)
     * @param boolean $showWeek Informa se deve exibir o dia da semana
     * @param string $separator Separador da data (Default /)
     * @return string
     */
    public function dateMesAnoToString($date, $language = "pt", $showWeek = false, $separator = "/") {

        if ($language == "pt") {
            setlocale(LC_TIME, 'ptb', 'pt_BR', 'portuguese-brazil', 'bra', 'brazil', 'pt_BR.utf-8', 'pt_BR.iso-8859-1', 'br');
            $stringConfig = $showWeek === true ? "%A, %B de %Y" : "%B de %Y";
        } else if ($language == "en") {
            setlocale(LC_ALL, "en-au");
            $stringConfig = "%B %dth, %Y";
        } else if ($language == "es") {
            setlocale(LC_ALL, "esp");
            $stringConfig = "%B %d, %Y";
        } else {
            setlocale(LC_TIME, 'ptb', 'pt_BR', 'portuguese-brazil', 'bra', 'brazil', 'pt_BR.utf-8', 'pt_BR.iso-8859-1', 'br');
            $stringConfig = $showWeek === true ? "%A, %d de %B de %Y" : "%d de %B de %Y";
        }

        $arrayData = explode($separator, $date);
        $string = strftime($stringConfig, mktime(0, 0, 0, $arrayData[0], 1, $arrayData[1]));

        return $string;
    }

    public function getMesExtenso($mes, $abreviado = false) {

        setlocale(LC_ALL, "pt_BR", "ptb");

        $param = $abreviado == false ? "b" : "B";

        $string = strftime("%" . $param, mktime(0, 0, 0, $mes, date("d"), date("Y")));

        return $string;
    }

    /**
     * Converte um Date para uma string do dia da semana
     * @param string $date Date a ser convertido no formato DD/MM/YYYY
     * @param string $separatorDate Separador da data. (Padrão "/")
     * @return string String do dia da semana. Ex.: Segunda-feira
     */
    public function dateToStringWeek($date, $separatorDate = "/") {

        $stringWeek = NULL;

        if (isValidData($date)) {

            setlocale(LC_TIME, 'ptb', 'pt_BR', 'portuguese-brazil', 'bra', 'brazil', 'pt_BR.utf-8', 'pt_BR.iso-8859-1', 'br');

            $dateList = explode($separatorDate, $date);

            $stringWeek = strftime("%A", mktime(0, 0, 0, $dateList[1], $dateList[0], $dateList[2]));
        }

        return $stringWeek;
    }

    /**
     * Converte um Datetime para uma string de tempo baseado na data/hora atual.
     * @param string $datetime Datetime a ser convertido no formato DD/MM/YYYY HH24:MM
     * @param string $separatorDate Separador da data. (Padrão "/")
     * @param string $separatorTime Separador da hora. (Padrão " ")
     * @return string String de tempo. Ex.: 20 minutos atrás
     */
    public function datetimeToStringTime($datetime, $separatorDate = "/", $separatorTime = " ") {

        $isValidDateTime = false;
        $stringTime = NULL;

        $datetimeList = explode($separatorTime, $datetime);

        if (count($datetimeList) == 2) {

            $hora = $datetimeList[1];
            $data = implode("-", array_reverse(explode($separatorDate, $datetimeList[0])));

            $datetime = $data . " " . $hora;

            $isValidDateTime = true;
        }

        if ($isValidDateTime === true) {

            $sdate = strtotime($datetime);
            $edate = strtotime('now');

            $time = $edate - $sdate;

            if ($time >= 0 && $time <= 59) {

                $stringTime = "há " . $time . " segundos";
            } elseif ($time >= 60 && $time <= 3599) {

                $pmin = floor($time / 60);

                $stringTime = "há " . $pmin . " minuto" . ($pmin > 1 ? "s" : NULL);
            } elseif ($time >= 3600 && $time <= 86399) {

                $phour = floor($time / 3600);

                $stringTime = "há " . $phour . " hora" . ($phour > 1 ? "s" : NULL);
            } elseif ($time >= 86400 && $time < 2592000) {

                $pday = ($time / 86400);

                if ($pday <= 1.5) { // Ontem
                    $stringTime = "Ontem às " . $hora;
                } else if ($pday <= 7) { // há uma semana
                    $date = implode("/", explode($separatorDate, $datetimeList[0]));

                    $stringTime = dateToStringWeek($date) . " às " . $hora;
                } else { // Há mais de uma semana
                    $stringTime = floor($pday) . " dias atrás";
                }
            } else if ($time >= 2592000) {

                $pmonth = $time / 2592000;

                $stringTime = floor($pmonth) . " meses atrás";
            }
        }

        return $stringTime;
    }

    /**
     * Calcula a idade através da data de nascimento
     * @access public
     * @param string $data_nascimento Data de nascimento no formato DD/MM/YYYY
     * @return int Retorna a idade
     */
    public function getIdadePorDataNascimento($data_nascimento) {

        $idade = 0;

        if (isValidData($data_nascimento)) {

            list($dia, $mes, $ano) = explode("/", $data_nascimento);

            $idade = date("Y") - $ano;
            $mes_diff = date("m") - $mes;
            $dia_diff = date("d") - $dia;

            if ($mes_diff < 0) {
                $idade--;
            } elseif (($mes_diff == 0) && ($dia_diff < 0)) {
                $idade--;
            }
        }

        return $idade;
    }
    
    /**
        * Function: sanitize
        * Returns a sanitized string, typically for URLs.
        *
        * Parameters:
        *     $string - The string to sanitize.
        *     $force_lowercase - Force the string to lowercase?
        *     $anal - If set to *true*, will remove all non-alphanumeric characters.
        */
     public function sanitize($str, $force_lowercase = true, $anal = false) {
           	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
                $clean = preg_replace("/[^a-zA-Z0-9\/_| -]/", '', $clean);
                $clean = strtolower(trim($clean, '-'));
                $clean = preg_replace("/[\/_| -]+/", '-', $clean);

                return $clean;
       }
    
    public function __clone() {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

    public function __wakeup() {
        trigger_error('Unserializing is not allowed.', E_USER_ERROR);
    }

    public static function get_format() {

        if (self::$instance instanceof Format === FALSE) {

            self::$instance = new Format();
        }

        return self::$instance;
    }
}