<?php
namespace Codando\System;

/**
* Classe para gerenciamento de javascript com o site
* @author  Luiz Antônio J. S. Thomas <luizz@luizz.com.br> 
* @copyright 2014 Luizz 
* @license http://www.luiz.com.br 
* @version 2.0 
*/

class JavaScript {
    
    /**
     * Instacia da Javascript
     */
    private static $instance = NULL;

    /**
     * Lista de m�dulos de JavaScript a ser carregado
     */
    private $callback = array();

    /**
     * Lista dos arquivos Javascript a serem carregados 
     */
    private $load = array('jquery');
    
    /**
     * Lista dos arquivos Javascript a serem carregados no body
     */
    private $loadBody = array();

    /**
     * C�digo JavaScript a ser inserido na p�gina (vai ser inserido no rodap�)
     */
    private $code = NULL;

    /**
     * Navegador do usuario
     */
    private $browser = array('versao' => 0, 'nome' => 'other');

    /**
     * Diretorio de javascript 
     */
    private $base_js = "js/";

    /**
     * Javascript reponsavel por criar os $load
     */
    private $js_callback = "require.js";

    // gerar versao
    private $versao = NULL;
    
    public function __call($name, $arguments) {
        return $this->_addCallback("app.".$name."(".implode(",",$arguments).");");
    }

    /**
     * Adicionar mais um chamada de funcao || objeto 
     * @param array || string $call
     * @return Javascript 
     */
    public function _addCallback($call) {

        if (is_array($call) === true) {
            foreach ($call as $callT) {
                if (in_array($callT, $this->callback) === false) {
                    $this->callback[] = $callT;
                }
            }
        } else {
            if (in_array($call, $this->callback) === false)
                $this->callback[] = $call;
        }

        return $this;
    }
    
    /**
     * Adicionar mais uma chamada de um script
     * @param array || string $load
     * @return Javascript 
     */
    public function _addLoad($load) {

        if (is_array($load) === true) {
            foreach ($load as $loadT) {
                if (in_array($loadT, $this->load) === false) {
                    $this->load[] = $loadT;
                }
            }
        } else {
            if (in_array($load, $this->load) === false)
                $this->load[] = $load;
        }

        return $this;
    }
    
    public function _addLoadBody($loadB){
        
         if (is_array($loadB) === true) {
            foreach ($loadB as $loadBT) {
                if (in_array($loadBT, $this->loadBody) === false) {
                    $this->loadBody[] = $loadBT;
                }
            }
        } else {
            if (in_array($loadB, $this->loadBody) === false)
                $this->loadBody[] = $loadB;
        }

        return $this;
    }
    
    public function _addCode($code) {
        $this->code .= "\n\t" . $code . "\n\t";
        return $this;
    }

    public function _print() {

        $source = NULL;
       
        $tempLoad = implode("-", $this->load);
        $tempCall = implode("\n ", $this->callback);

        $source = "require(['app'], function(app){ app(); });";


        $base64 = base64_encode($source);

        $return = "\t<script type=\"text/javascript\" data-main=\"js/app\" src=\"" . $this->base_js . $this->js_callback . $this->versao . "\" data-no-instant></script>\n";
        
        foreach ($this->loadBody as $loadyB) {
            $return .= "\t<script type=\"text/javascript\" src=\"" . $loadyB . "\" data-no-instant></script>\n";
        }

        $return .= "\t<script type=\"text/javascript\" " . ($this->browser['nome'] == 'IE' && $this->browser['versao'] < 9 ? NULL : "src=\"data:text/javascript;base64,$base64\"") . " defer data-no-instant>" . ($this->browser['nome'] == 'IE' && $this->browser['versao'] < 9 ? $source : NULL) . "</script>\n";

        return $return;
    }

    public function __construct($js_callback = "require.js", $base_js = "js/") {
        
        $_config = \Codando\App::getConfig('site');
        
        if (function_exists('browser') === true) {
            $this->browser = browser();
        }
        
        $this->versao = $_SERVER["REMOTE_ADDR"] == "127.0.0.1" ? "?v=".(md5(uniqid())):NULL;
        
        $this->js_callback = $js_callback;
        
        $this->base_js = $_config['path'] . $base_js;
        
        $this->versao = '?v='.$_config['version'];
        
        unset($_config);
    }

    public function __destruct() {}

    public function __toString() {
        return $this->_print();
    }

    public function __clone() {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

    public function __wakeup() {
        trigger_error('Unserializing is not allowed.', E_USER_ERROR);
    }
    
    public static function get_javascript() {
        static $instance = null;

        return $instance ? : $instance = new static;
    }

}
