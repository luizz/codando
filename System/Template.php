<?php

namespace Codando\System;

/**
 * Classe Template 
 * /
 * @author  Luiz Antônio J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Template {

    private $output = null;
    private $time;
    private $cache;
    private $dir;
    private $dir_cache;
    private $ext;
    private $minify;
    private $config;
    private $httpCg = false;

    public function __construct() {

        $tpl_config = \Codando\App::getConfig('template');
        $this->config = \Codando\App::getConfig('site');
        
        if (is_array($tpl_config)) {
            $this->output = NULL;
            $this->dir = COD_DIR_APP . '/' . $tpl_config['dir'] . '/';
            $this->dir_cache = COD_DIR_APP . '/' . $tpl_config['dir_cache'] . '/';
            $this->time = $tpl_config['time'];
            if (defined('GOOBOT') === TRUE && GOOBOT === TRUE) {
                $this->cache = false;
            } else {
                $this->cache = $tpl_config['cache'];
            }
            $this->ext = '.' . (isset($tpl_config['ext']) ? $tpl_config['ext'] : 'phtml');
            $this->minify = $tpl_config['minify'];
        } else {

            exit('Template not config');
        }

        $tpl_config = NULL;
    }

    public function __destruct() {
        $this->config = NULL;
        unset($this->output);
    }

    public function http($every = 60){

//        // emulate regularly updating document
//        $lastModified = round(time()/$every)*$every - $every;
//
//        $offset = 60 * 60 * 24 * 7; // Cache for 1 weeks
//        header('Expires: ' . gmdate("D, d M Y H:i:s", time() + $offset) . ' GMT');
//
//        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= $modified) {
//
//            header("HTTP/1.0 304 Not Modified");
//            header('Cache-Control:');
//        } else {
//
//             
//        }
        return $this;
    }

    public function outHttp(){
        //$this->httpCg->sendHeaders();
    }

    public function escape($string, $charset = 'UTF-8') {
        return htmlspecialchars($string, ENT_QUOTES | ENT_HTML5, $charset);
    }

    public function isExist($name) {
        return file_exists($this->dir . $name . $this->ext);
    }

    public function display($template, array $vars = array(), $out = TRUE) {

        if (is_array($template)) {
            foreach ($template as $key => $value) {
                $this->display($key, (array) $value);
            }
        } else {
            
            if(defined('COD_APP_MOBILE') && COD_APP_MOBILE == TRUE && $this->isExist( "mobile/" . $template )){
                $template = "mobile/" . $template;
            }
            
            $this->output = NULL;

            if ($this->isExist($template)) {

                if ($this->cache($template) === true || count($vars) > 0) {

                    $config = $this->config;

                    extract($vars);
                    ob_start();

                    include($this->dir . $template . $this->ext);

                    $this->output = $this->minify === true ? $this->minify(ob_get_clean()) : ob_get_clean();

                    if (count($vars) === 0) {
                        $this->createCache($template);
                    }
                }

//                if (defined('GOOBOT') === TRUE && GOOBOT === FALSE) {
//                    $this->output = $this->lazyload($this->output);
//                }
            }

            if ($out === TRUE) {
                print $this->output;
            } else {
                return $this->output;
            }
        }
    }

    public function cache($name, $callback = NULL, $time = FALSE, $parametro = array()) {

        if ($callback === NULL && $this->cache !== true) {
            return TRUE;
        }

        $time = $time === FALSE ? $this->time : $time;

        $file = $this->dir_cache . $name . '.phcache';

        if (file_exists($file) === FALSE) {

            if ($callback !== NULL) {

                $returnCall = call_user_func($callback, $parametro);

                $this->createCache($name, $returnCall);

                return $returnCall;
            } else {

                return TRUE;
            }
        }

        $now = time();
        $since = filemtime($file);

        if ($time !== NULL && ($now - $since > $time)) {

            if ($callback !== NULL) {

                $returnCall = call_user_func($callback, $parametro);

                $this->createCache($name, $returnCall);

                return $returnCall;
            } else {

                return TRUE;
            }
        }

        if ($callback !== NULL) {

            return $this->readCache($name, TRUE);
        } else {

            $this->readCache($name);

            return FALSE;
        }
    }

    private function createCache($name, $output = NULL) {

        $output = $output !== NULL ? $output : $this->output;

        if (!$fp = @fopen($this->dir_cache . $name . '.phcache', 'wb')) {
            return FALSE;
        }

        flock($fp, LOCK_EX);
        fwrite($fp, serialize($output));
        flock($fp, LOCK_UN);
        fclose($fp);
        $fp = NULL;
        unset($fp);
    }

    private function readCache($name, $return = FALSE) {

        $file = $this->dir_cache . $name . '.phcache';

        if (!file_exists($file)) {
            return FALSE;
        }

        if (function_exists('file_get_contents')) {
            $this->output = unserialize(file_get_contents($file));
        }

        if (!$fp = @fopen($file, 'rb')) {
            return FALSE;
        }

        flock($fp, LOCK_SH);

        $data = '';
        if (filesize($file) > 0) {
            $data = fread($fp, filesize($file));
        }

        flock($fp, LOCK_UN);
        fclose($fp);
        $fp = NULL;
        unset($fp);

        if ($return === FALSE) {

            $this->output = unserialize($data);
        } else {

            return unserialize($data);
        }
    }

    private function replacer($src, $searc, $find, $replace) {

        preg_match_all($searc, $src, $result, PREG_OFFSET_CAPTURE);

        foreach ($result[0] as $entry) {
            $org = $entry[0];
            $rep = preg_replace($find, $replace, $entry[0]);
            $org = "/" . str_replace(array("=", ":", "/", ".", "-", "_", '"', "'", " "), array("\=", "\:", "\/", "\.", "\-", "\_", '\"', "\'", "\ "), $org) . "/";
            $src = preg_replace($org, $rep, $src);
        }

        return $src;
    }

    private function lazyload($buffer) {

        $searchIMG = '/src\=\"([^\s]+(?=\.(bmp|gif|jpeg|jpg|png))\.\2)\"/';

        return $this->replacer($buffer, $searchIMG, '/src/', 'data-original');
    }

    private function minify($buffer) {

        $search = array(
            '/\>[^\S ]+/s',
            '/[^\S ]+\</s',
            '/(\s)+/s',
            '/(<a href="http:[^"]+")>/is'
        );
        
        $replace = array(
            '>',
            '<',
            '\\1',
            '\\1 target="_blank">'
        );

        return preg_replace($search, $replace, $buffer);
    }

    public static function get_tpl() {
        static $instance = null;

        return $instance ? : $instance = new static;
    }

}
?>