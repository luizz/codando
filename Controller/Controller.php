<?php

namespace Codando\Controller;

use Codando\System\Paginar;

/**
 * 	E-mail: luizz@luizz.com.br
 * 	Copyright 2014 - Todos os direitos reservados
 */
abstract class Controller {

    /**
     * Lista de mensagens
     * @var array<string>
     */
    protected $messageList;

    /**
     * Informa se ocorreu algum erro no processamento da requisição
     * @var bool
     */
    protected $error;

    /**
     * Objeto que representa um Modulo
     * @var stdClass
     */
    protected $modulo;

    /**
     * Objeto que representa um Modulo
     * @var <array>stdClass
     */
    protected $moduloList;
    protected $paginar;
    protected $isPaginar;
    private $setLoadAllDependencies;
    protected $quantidadePorPagina = 20;
    protected $quantidadePaginacaoPorPagina = 10;

    protected abstract function _doRoute();

    protected function _getRoute($action = "action") {

        $action = input()->_toRequest($action);

        $action = strtolower(substr($action, 0, 1)) . substr($action, 1);

        return $action;
    }

    public function getModulo($returnNewObject = false) {

        if ($this->modulo == NULL && $returnNewObject !== false) {

            $this->modulo = new $returnNewObject;
        }

        return $this->modulo;
    }

    public function getModuloList() {

        return (is_array($this->moduloList) ? $this->moduloList : array());
    }

    public function isPaginar($isPaginar = NULL) {
        return $this->isPaginar = $isPaginar !== NULL ? $isPaginar : $this->isPaginar;
    }

    public function setPaginar($quantidade) {

        $page = (int)input()->_toRequest('p', '%d');
        
        $this->paginar = new \Codando\System\Paginar($quantidade, $this->getRegistrosPorPagina(), $page, $this->quantidadePaginacaoPorPagina);
    }

    public function getOffSetPaginar() {
        return $this->paginar->getOffSet();
    }

    public function getLimitPaginar() {
        return $this->paginar->getLimit();
    }

    public function getStartPaginar() {

        if (is_instanceof('Codando\System\Paginar', $this->paginar) === TRUE) {
            return $this->paginar->getPagStart();
        }
        return NULL;
    }

    public function getEndPaginar() {

        if (is_instanceof('Codando\System\Paginar', $this->paginar) === TRUE) {
            return $this->paginar->getPagEnd();
        }
        return NULL;
    }

    public function getTotalPaginar() {

        if (is_instanceof('Codando\System\Paginar', $this->paginar) === TRUE) {
            return $this->paginar->getNumPages();
        }
        return NULL;
    }

    public function getCurrentPaginar() {

        if (is_instanceof('Codando\System\Paginar', $this->paginar) === TRUE) {
            return $this->paginar->getPagNum();
        }

        return NULL;
    }

    public function setRegistrosPorPagina($quantidadePorPagina) {
        $this->quantidadePorPagina = $quantidadePorPagina;
    }

    protected function getRegistrosPorPagina() {
        return $this->quantidadePorPagina;
    }

    public function setQuantidadePaginacaoPorPagina($quantidadePaginacaoPorPagina) {
        $this->quantidadePaginacaoPorPagina = $quantidadePaginacaoPorPagina;
    }

    public function setUrl($urlBase) {

        if (is_instanceof('Codando\System\Paginar', $this->paginar) === TRUE) {

            $this->paginar->setBaseUrl($urlBase);
        }
    }

    public function getPaginacao() {

        return (is_instanceof('Codando\System\Paginar', $this->paginar) === TRUE ? $this->paginar->getPaginar() : NULL);
    }

    public function getMessageList() {

        return is_array($this->messageList) ? $this->messageList : array();
    }

    public function setLoadAllDependencies($loadAllDependencies) {
        $this->setLoadAllDependencies = $loadAllDependencies;
    }

    public function isError() {

        return $this->error;
    }

    public function clearMessages() {
        $this->setLoadAllDependencies = TRUE;
        $this->messages = array();
        $this->error = false;
    }

    abstract public function __construct($a = false);

    abstract public function _initialize();
}