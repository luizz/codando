<?php
namespace Codando\Controller;

class Pagina extends Controller {

        public function _doRoute() {

            $this->cleanStateController();

            $acao = $this->_getRoute();

            switch ($acao) {

                case "carregar":

                    $this->_load();

                    break;

                case "listar":

                    $this->_list();

                    break;

            }
        }
        
        public function _load() {


            $pag_id = input()->_toGet('pag_id', '%d', true);
            
            if($pag_id !== NULL){
                
                $this->modulo = app()->loadModulo('pagina', array('id_pagina = :id_pagina', array('id_pagina'=> $pag_id)));
                
                if (is_instanceof('Codando\Modulo\Pagina', $this->modulo)  === False) {

                    $this->error = true;
                    $this->messageList = "Pagina não encontrado";
                }
            } else {

                $this->error = true;
                $this->messageList = input()->getMsg();
            }
        }
       
        public function cleanStateController() {

            $this->clearMessages();
            $this->modulo = NULL;
            $this->moduloList = array();
        }

        public function _initialize() {

            $this->_doRoute();
        }

        public function __construct($autoInitialize = false) {

            if ($autoInitialize === true) {
                $this->_initialize();
            }
        }

        public function __destruct() {

            unset($this->modulo, $this->moduloList);
        }

    }
    