<?php

namespace Codando\Controller;

class Contato extends Controller {
    
    public $feedback;

    public function _doRoute() {

        $this->cleanStateController();

        $acao = $this->_getRoute();

        switch ($acao) {

            case "adicionar":

                $this->_insert();

                break;
        }
    }
    
    public function _insert() {
       
        $_contato = array(
            'nome' => 'xss%s',
            'email' => 'EMAIL',
            'assunto' => 'xss%s',
            'mensagem' => 'xss%s'
        );

        $contato = input()->_toPost($_contato, true);

        if (is_array($contato) === TRUE && $this->_isSend($_contato['email'], $_contato['mensagem']) === false) {
            
            $contato['data'] = date('Y-m-d');

            $id_contato = db()->insert('contato', $contato);

            $this->_load($id_contato);

            if (is_instanceof('Codando\Modulo\Contato', $this->modulo) === FALSE) {

                $this->error = true;
                $this->messageList[] = $this->feedback['pt'][1];
                
                return FALSE;
            } 

            $this->_send();
            
            return TRUE;
        }
        
        $this->error = TRUE;
        $this->messageList[] =  $this->feedback['pt'][0];
    }

    public function _load($con_id) {

        $this->modulo = app()->loadModulo('contato', array('id_contato = :id_contato', array('id_contato' => $con_id)));

        if (is_instanceof('Codando\Modulo\Contato', $this->modulo) === False) {

            $this->error = true;
            $this->messageList[] = $this->feedback['pt'][2];
        }
    }
    
    public function _isSend($email, $mensagem) {

        $this->modulo = app()->loadModulo('contato', array('MD5(mensagem) = :mensagem AND email = :email ', array( 'mensagem' => md5($mensagem), 'email' => $email )));

        if (is_instanceof('Codando\Modulo\Contato', $this->modulo) === TRUE) {

            $this->messageList[] = $this->feedback['pt'][3];
            return TRUE;
        }
        
        return FALSE;
    }

    public function _send() {

        $_config = \Codando\App::getConfig('emailsend');

        $deparmento = app()->loadModulo('departamento', array('id_departamento = :id_departamento' , array('id_departamento' => $_config['departament_contato'])));

        if (is_instanceof('Codando\Modulo\Departamento', $deparmento) === TRUE) {
            
            // Create the Transport
            $transport = \Swift_SmtpTransport::newInstance();

            /* @var $contato \Codando\Modulo\Contato */
            $contato = $this->modulo;
            
            $emailArray = explode(',', $deparmento->getEmail());
            
            $emailFields = array(
                'Nome' => $contato->getNome(),
                'Telefone' => $contato->getTelefone(),
                'Email' => $contato->getEmail(),
                'Mensagem' => nl2br($contato->getMensagem()));

            $html_email = tpl()->display('contato_send', array('emailFields' => $emailFields), false);

            $mailer = \Swift_Mailer::newInstance($transport);

            $message = \Swift_Message::newInstance('Novo Contato')        
                        ->setFrom(array('nao-responda@papoempreendedor.net' => 'Novo Contato'))
                        ->setTo($emailArray)
                        ->addPart($html_email, 'text/html');

            if ($mailer->send($message)) {

                return TRUE;
            } else {

                return FALSE;
            }
        } else {

            return FALSE;
        }
    }

    public function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function _initialize() {

        $this->_doRoute();
        
    }

    public function __construct($autoInitialize = false) {

        if ($autoInitialize === true) {
            $this->_initialize();
        }
        
        $this->feedback = array(
            'pt' => array('Preencha os campos obrigatórios(*), corretamente!', 'Contato não inserido', 'Contato não encontrado', 'Contato já foi enviado anteriormente.'),
            'en' => array('Complete the required (*) fields correctly!', 'Not inserted Contact', 'Contact not met'),
            'es' => array('Preencha os campos obrigatórios(*), corretamente!', 'No inserido Contact', 'Contact no alcanzada'),
        );
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
