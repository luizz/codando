<?php

function is_instanceof($instanceName, $object) {
    return ($object instanceof $instanceName === TRUE && get_class($object) === $instanceName);
}

function is_instanceofOrNew($instanceName, $object) {
    return is_instanceof($instanceName, $object) ? $object:new $instanceName;
}

function tableToclass($table) {
    return ucfirst(str_replace("_", "", strtolower($table)));
}

function filetohtml($arquivo, $w, $h, $link = NULL){
           
    $html = NULL;
    
    /* @var $arquivo Codando\Modulo\Arquivo */
    if(is_instanceof('Codando\Modulo\Arquivo', $arquivo)){
        
        $ext = pathinfo(COD_DIR_ROOT . '/app/_arquivos/' . $arquivo->getBase()  . $arquivo->getUrl(), PATHINFO_EXTENSION);

      
            if(strtolower($ext) == 'swf'){
              $html = '<div style="display:block; position:relative; width:'.$w.'px; height:'.$h.'px; z-index:0">'.
                        '<!--[if !IE]> -->'.
                           '<object type="application/x-shockwave-flash" data="'.$arquivo->getImage().'" width="'.$w.'" height="'.$h.'">'.
                            '<!-- <![endif]--><!--[if IE]><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="300" height="250"><param name="movie" value="'.$arquivo->getImage().'" /><!-->'.
                               '<param name="quality" value="high"><param name="wmode" value="transparent">'.
                           '</object>'.
                        '<!-- <![endif]-->'.
                         '<div style="display:block; position:absolute; left:0px; top:0px; width:'.$w.'px; height:'.$h.'px; z-index:100">'.
                             '<a '. href_target($link) .'><img width="'.$w.'" height="'.$h.'" src="/img/btran.gif" border="0" alt=""></a>'.
                         '</div>'.
                     '</div>';
            }else{
                   $html = '<a '. href_target($link) .' style="display:block; width:'.$w.'px; height:'.$h.'px;">'.
                            '<img src="'.$arquivo->getImage().'" width="'.$w.'"  height="'.$h.'" />'.
                           '</a>';
            }
    }
    
    return $html;
    
}

function inull($i, $null){
    return is()->nul($i) == FALSE ? $null:NULL;
}

function redirecionar($redirectTo = NULL, $returnTo = NULL) {
    
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
        
        return NULL;
    }
    
    if ($redirectTo != NULL) {

        if ($returnTo != NULL) {
            $_SESSION["controller_return_to"] = $returnTo;
        }
        
        //db()->_disconect();

        if (headers_sent() === false) {
            header('Location: ' . $redirectTo);
            exit;
        } else {
            echo '<script type="text/javascript">'
            , 'window.location.href="'  . $redirectTo . '";'
            , '</script>'
            , '<noscript>'
            , '<meta http-equiv="refresh" content="0;url='  . $redirectTo . '" />'
            , '</noscript>';
            exit;
        }
        
    } else {

        if (isset($_SESSION["controller_return_to"])) {

            $redirectTo = $_SESSION["controller_return_to"];
            unset($_SESSION["controller_return_to"]);
            
            db()->_disconect();
            
            if (headers_sent() === false) {
                header('Location: ' . $redirectTo);
                exit;
            } else {
                echo '<script type="text/javascript">'
                , 'window.location.href="'  . $redirectTo . '";'
                , '</script>'
                , '<noscript>'
                , '<meta http-equiv="refresh" content="0;url='  . $redirectTo . '" />'
                , '</noscript>';
                exit;
            }

        }
    }
}

/**
 * Identificar o navegador.
 */
function browser() {

    $browser = array('versao' => 0, 'nome' => 'other');

    $useragent = isset($_SERVER['HTTP_USER_AGENT']) === true ? $_SERVER['HTTP_USER_AGENT'] : NULL;

    if (preg_match('|MSIE ([0-9].[0-9]{1,2})|', $useragent, $matched)) {
        $browser['versao'] = $matched[1];
        $browser['nome'] = 'IE';
    } elseif (preg_match('|Opera/([0-9].[0-9]{1,2})|', $useragent, $matched)) {
        $browser['versao'] = $matched[1];
        $browser['nome'] = 'Opera';
    } elseif (preg_match('|Firefox/([0-9\.]+)|', $useragent, $matched)) {
        $browser['versao'] = $matched[1];
        $browser['nome'] = 'Firefox';
    } elseif (preg_match('|Chrome/([0-9\.]+)|', $useragent, $matched)) {
        $browser['versao'] = $matched[1];
        $browser['nome'] = 'Chrome';
    } elseif (preg_match('|Safari/([0-9\.]+)|', $useragent, $matched)) {
        $browser['versao'] = $matched[1];
        $browser['nome'] = 'Safari';
    }

    return $browser;
}

function addURLParameter($url, $paramName, $paramValue) {
     $url_data = parse_url($url);
     if(!isset($url_data["query"]))
         $url_data["query"]="";

     $params = array();
     parse_str($url_data['query'], $params);
     $params[$paramName] = $paramValue;   
     $url_data['query'] = http_build_query($params);
     return build_url($url_data);
}

 function build_url($url_data) {
     $url="";
     if(isset($url_data['host']))
     {
         $url .= $url_data['scheme'] . '://';
         if (isset($url_data['user'])) {
             $url .= $url_data['user'];
                 if (isset($url_data['pass'])) {
                     $url .= ':' . $url_data['pass'];
                 }
             $url .= '@';
         }
         $url .= $url_data['host'];
         if (isset($url_data['port'])) {
             $url .= ':' . $url_data['port'];
         }
     }
     $url .= $url_data['path'];
     if (isset($url_data['query'])) {
         $url .= '?' . $url_data['query'];
     }
     if (isset($url_data['fragment'])) {
         $url .= '#' . $url_data['fragment'];
     }
     return $url;
 }

 function href_target($link) {
  return  is()->nul($link) === FALSE ? (strpos( $link, $_SERVER['HTTP_HOST'] ) === false ? " target=\"_blank\" itemprop=\"url\" rel=\"nofollow\" href=\"".$link."\" ":" href=\"".$link."\" "):NULL;
}
 
/**
 * Obter a instacia da class App
 * @return \Codando\App
 */
function app(){
  return Codando\App::getInstance();
}

/**
 * @return \Codando\System\JavaScript
 */
function js() {
    return Codando\System\JavaScript::get_javascript();
}

/**
 * @return \Codando\System\Meta
 */
function meta() {
    return Codando\System\Meta::get_meta();
}

/**
 * @return \Codando\System\Is
 */
function is(){
    return Codando\System\Is::get_is();
}

/**
 * @return \Codando\System\Db
 */
function db(){
    return Codando\System\Db::get_db();
}

/**
 * @return \Codando\System\Request
 */
function input(){
  return  Codando\System\Request::get_request();
}

/**
 * @return \Codando\System\XSS
 */
function xss(){
  return  Codando\System\XSS::getInstance();
}

/**
 * @return \Codando\System\String
 */
function string(){
    return Codando\System\String::get_string();
}

/**
 * @return \Codando\System\Format
 */
function format(){
    return Codando\System\Format::get_format();
}

/**
 * @return \Codando\System\Erro
 */
function erro(Exception $e){
    return Codando\System\Erro::get_erro()->stackTrace($e);
}

/**
 * @return \Codando\System\Template
 */
function tpl(){
    return Codando\System\Template::get_tpl();
}

/**
 * @return \Codando\System\Css
 */
function css(){
    return Codando\System\Css::get_css();
}

/**
 * @return \Codando\System\CriptBasic
 */
function cryp() {
    return Codando\System\CriptBasic::get_cryp();
}

function timesleep($increment = 1){
        $input = input();
        $sleep = $increment + ($input->getSession('sleep','%d') !== NULL ? $input->getSession('sleep','%d'):0);
        $input->setSession('sleep', $sleep);
        if($sleep > $increment) sleep($sleep);
}

/**
 * get youtube video ID from URL
 *
 * @param string $url
 * @return string Youtube video id or FALSE if none found. 
 */
function youtube_id_from_url($url) {
    $url = preg_replace('~
        # Match non-linked youtube URL in the wild. (Rev:20111012)
        https?://         # Required scheme. Either http or https.
        (?:[0-9A-Z-]+\.)? # Optional subdomain.
        (?:               # Group host alternatives.
          youtu\.be/      # Either youtu.be,
        | youtube\.com    # or youtube.com followed by
          \S*             # Allow anything up to VIDEO_ID,
          [^\w\-\s]       # but char before ID is non-ID char.
        )                 # End host alternatives.
        ([\w\-]{11})      # $1: VIDEO_ID is exactly 11 chars.
        (?=[^\w\-]|$)     # Assert next char is non-ID or EOS.
        (?!               # Assert URL is not pre-linked.
          [?=&+%\w]*      # Allow URL (query) remainder.
          (?:             # Group pre-linked alternatives.
            [\'"][^<>]*>  # Either inside a start tag,
          | </a>          # or inside <a> element text contents.
          )               # End recognized pre-linked alts.
        )                 # End negative lookahead assertion.
        [?=&+%\w-]*        # Consume any URL (query) remainder.
        ~ix', 
        '$1',
        $url);
    return $url;
}


/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
define('FOPEN_READ','rb');
define('FOPEN_READ_WRITE','r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE','wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE','w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE','ab');
define('FOPEN_READ_WRITE_CREATE','a+b');
define('FOPEN_WRITE_CREATE_STRICT','xb');
define('FOPEN_READ_WRITE_CREATE_STRICT','x+b');

// ------------------------------------------------------------------------

/**
 * Read File
 *
 * Opens the file specfied in the path and returns it as a string.
 *
 * @access	public
 * @param	string	path to file
 * @return	string
 */
if ( ! function_exists('read_file'))
{
	function read_file($file)
	{
		if ( ! file_exists($file))
		{
			return FALSE;
		}

		if (function_exists('file_get_contents'))
		{
			return file_get_contents($file);
		}

		if ( ! $fp = @fopen($file, FOPEN_READ))
		{
			return FALSE;
		}

		flock($fp, LOCK_SH);

		$data = '';
		if (filesize($file) > 0)
		{
			$data =& fread($fp, filesize($file));
		}

		flock($fp, LOCK_UN);
		fclose($fp);

		return $data;
	}
}

// ------------------------------------------------------------------------

/**
 * Write File
 *
 * Writes data to the file specified in the path.
 * Creates a new file if non-existent.
 *
 * @access	public
 * @param	string	path to file
 * @param	string	file data
 * @return	bool
 */
if ( ! function_exists('write_file'))
{
	function write_file($path, $data, $mode = FOPEN_WRITE_CREATE_DESTRUCTIVE)
	{
		if ( ! $fp = @fopen($path, $mode))
		{
			return FALSE;
		}

		flock($fp, LOCK_EX);
		fwrite($fp, $data);
		flock($fp, LOCK_UN);
		fclose($fp);

		return TRUE;
	}
}

// ------------------------------------------------------------------------



/**
 * Write File
 *
 * Writes data to the file specified in the path.
 * Creates a new file if non-existent.
 *
 * @access	public
 * @param	string	path to file
 * @param	string	file data
 * @return	bool
 */
if ( ! function_exists('write_file_csv'))
{
	function write_file_csv($path, $data, $mode = FOPEN_WRITE_CREATE_DESTRUCTIVE)
	{
		if ( ! $fp = @fopen($path, $mode))
		{
			return FALSE;
		}

		flock($fp, LOCK_EX);
		foreach($data as $array) {
                    fputcsv($fp, $array);
                }
		flock($fp, LOCK_UN);
		fclose($fp);

		return TRUE;
	}
}

/**
 * Delete Files
 *
 * Deletes all files contained in the supplied directory path.
 * Files must be writable or owned by the system in order to be deleted.
 * If the second parameter is set to TRUE, any directories contained
 * within the supplied base directory will be nuked as well.
 *
 * @access	public
 * @param	string	path to file
 * @param	bool	whether to delete any directories found in the path
 * @return	bool
 */
if ( ! function_exists('delete_files'))
{
	function delete_files($path, $del_dir = FALSE, $level = 0)
	{
		// Trim the trailing slash
		$path = rtrim($path, DIRECTORY_SEPARATOR);

		if ( ! $current_dir = @opendir($path))
		{
			return FALSE;
		}

		while (FALSE !== ($filename = @readdir($current_dir)))
		{
			if ($filename != "." and $filename != "..")
			{
				if (is_dir($path.DIRECTORY_SEPARATOR.$filename))
				{
					// Ignore empty folders
					if (substr($filename, 0, 1) != '.')
					{
						delete_files($path.DIRECTORY_SEPARATOR.$filename, $del_dir, $level + 1);
					}
				}
				else
				{
					unlink($path.DIRECTORY_SEPARATOR.$filename);
				}
			}
		}
		@closedir($current_dir);

		if ($del_dir == TRUE AND $level > 0)
		{
			return @rmdir($path);
		}

		return TRUE;
	}
}

// --------------------------------------------------------------------

/**
 * Symbolic Permissions
 *
 * Takes a numeric value representing a file's permissions and returns
 * standard symbolic notation representing that value
 *
 * @access	public
 * @param	int
 * @return	string
 */
if ( ! function_exists('symbolic_permissions'))
{
	function symbolic_permissions($perms)
	{
		if (($perms & 0xC000) == 0xC000)
		{
			$symbolic = 's'; // Socket
		}
		elseif (($perms & 0xA000) == 0xA000)
		{
			$symbolic = 'l'; // Symbolic Link
		}
		elseif (($perms & 0x8000) == 0x8000)
		{
			$symbolic = '-'; // Regular
		}
		elseif (($perms & 0x6000) == 0x6000)
		{
			$symbolic = 'b'; // Block special
		}
		elseif (($perms & 0x4000) == 0x4000)
		{
			$symbolic = 'd'; // Directory
		}
		elseif (($perms & 0x2000) == 0x2000)
		{
			$symbolic = 'c'; // Character special
		}
		elseif (($perms & 0x1000) == 0x1000)
		{
			$symbolic = 'p'; // FIFO pipe
		}
		else
		{
			$symbolic = 'u'; // Unknown
		}

		// Owner
		$symbolic .= (($perms & 0x0100) ? 'r' : '-');
		$symbolic .= (($perms & 0x0080) ? 'w' : '-');
		$symbolic .= (($perms & 0x0040) ? (($perms & 0x0800) ? 's' : 'x' ) : (($perms & 0x0800) ? 'S' : '-'));

		// Group
		$symbolic .= (($perms & 0x0020) ? 'r' : '-');
		$symbolic .= (($perms & 0x0010) ? 'w' : '-');
		$symbolic .= (($perms & 0x0008) ? (($perms & 0x0400) ? 's' : 'x' ) : (($perms & 0x0400) ? 'S' : '-'));

		// World
		$symbolic .= (($perms & 0x0004) ? 'r' : '-');
		$symbolic .= (($perms & 0x0002) ? 'w' : '-');
		$symbolic .= (($perms & 0x0001) ? (($perms & 0x0200) ? 't' : 'x' ) : (($perms & 0x0200) ? 'T' : '-'));

		return $symbolic;
	}
}

// --------------------------------------------------------------------

/**
 * Octal Permissions
 *
 * Takes a numeric value representing a file's permissions and returns
 * a three character string representing the file's octal permissions
 *
 * @access	public
 * @param	int
 * @return	string
 */
if ( ! function_exists('octal_permissions'))
{
	function octal_permissions($perms)
	{
		return substr(sprintf('%o', $perms), -3);
	}
}
