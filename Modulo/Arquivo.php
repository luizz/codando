<?php

namespace Codando\Modulo;

class Arquivo {

    private $id_arquivo;
    private $url = COD_APP_404_IMG;
    private $id_est;
    private $id_modulo;
    private $ordem;
    private $legenda;
    private $fonte;
    private $base = 'semimagem/';
    private $link;

    public function getId() {
        return (int) $this->id_arquivo;
    }

    public function getUrl($w = 0, $h = 0, $crop = 0) {
        return $w > 0 && $h > 0 && $crop != 0 ? COD_APP_CDN . "imagens/" . $this->base . "$w/$h/$crop/" . $this->url : ($w > 0 && $h > 0 ? COD_APP_CDN . "imagens/" . $this->base . "$w/$h/" . $this->url : $this->url);
    }

    public function getIdest() {
        return $this->id_est;
    }

    public function getIdmodulo() {
        return $this->id_modulo;
    }

    public function getOrdem() {
        return $this->ordem;
    }

    public function getLegenda() {
        return $this->legenda;
    }

    public function getFonte() {
        return $this->fonte;
    }

    public function getBase() {
        return $this->base;
    }

    public function getLink() {
        return $this->link;
    }

    public function setId($id_arquivo) {
        $this->id_arquivo = (int) $id_arquivo;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function setIdest($id_est) {
        $this->id_est = $id_est;
    }

    public function setIdmodulo($id_modulo) {
        $this->id_modulo = $id_modulo;
    }

    public function setOrdem($ordem) {
        $this->ordem = $ordem;
    }

    public function setLegenda($legenda) {
        $this->legenda = $legenda;
    }

    public function setFonte($fonte) {
        $this->fonte = $fonte;
    }

    public function setBase($base) {
        $this->base = $base . '/';
    }

    public function setLink($link) {
        $this->link = $link;
    }

    public function getLinkOrImg($w = 0, $h = 0, $crop = NULL) {
        
        if($this->link != NULL){
            return $this->link;
        }
        
        return $this->getImage($w, $h, $crop);
    }

    public function getImage($w = 0, $h = 0, $crop = NULL) {

        $url = COD_APP_CDN . "imagens/" . $this->base;

        if ($w > 0) {
            $url .= $w . "/";
        }

        if ($h > 0) {
            $url .= $h . "/";
        }

        if ($crop != NULL) {
            $url .= $crop . "/";
        }

        return $url . $this->url;
    }

    public function isEquals($arquivo) {
        return ($arquivo instanceof Arquivo && $this->id_arquivo == $arquivo->getId());
    }

}
