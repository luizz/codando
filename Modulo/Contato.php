<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Contato 
 * /
 * @author  Luiz Antônio J. S. Thomas <luizz@luizz.com.br>
 * @copyright  Luizz
 * @license www.luiz.com.br
 * @package Codando
 */
class Contato {

    private $id_contato;
    private $nome;
    private $email;
    private $telefone;
    private $mensagem;
    private $data;

    public function getId() {
        return (int) $this->id_contato;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getTelefone() {
        return $this->telefone;
    }

    public function getMensagem() {
        return $this->mensagem;
    }

    public function getData($format = 'd/m/Y') {
        return $this->data !== NULL ? date($format, strtotime($this->data)) : NULL;
    }

    public function setId($id_contato) {
        $this->id_contato = (int) $id_contato;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    public function setMensagem($mensagem) {
        $this->mensagem = $mensagem;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Contato && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_contato;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        unset($this);
    }

}

;
?>